package com.itlg.movieinventory.repository;

import com.itlg.movieinventory.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Movie findById(long id);
}
