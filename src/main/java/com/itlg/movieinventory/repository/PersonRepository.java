package com.itlg.movieinventory.repository;

import com.itlg.movieinventory.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findById(long id);

}
