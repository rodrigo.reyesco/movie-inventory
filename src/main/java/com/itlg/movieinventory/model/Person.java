package com.itlg.movieinventory.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonPropertyOrder("0")
    private Long id;

    @JsonProperty(value = "first-name")
    @Column(name = "first_name")
    @JsonPropertyOrder("1")
    private String firstName;

    @JsonProperty(value = "last-name")
    @Column(name = "last_name")
    @JsonPropertyOrder("2")
    private String lastName;

    @JsonProperty(value = "birthdate")
    @Column(name = "birthdate")
    @JsonPropertyOrder("3")
    private Date birthDate;

    @JsonProperty(value = "has-insurance")
    @Column(name = "has_insurance")
    @JsonPropertyOrder("4")
    private Boolean hasInsurance;

    @ManyToMany(cascade = {
            CascadeType.MERGE,
            CascadeType.PERSIST
    })
    @JoinTable(
            name = "person_movie",
            joinColumns = {@JoinColumn(name = "person_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    @JsonPropertyOrder("5")
    private Set<Movie> movies;

    public Person() {

    }

    public Person(Long id) {
        this.id = id;
    }

    public Person(String firstName, String lastName, Date birthDate, Boolean hasInsurance, Set<Movie> movies) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.hasInsurance = hasInsurance;
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Boolean getHasInsurance() {
        return hasInsurance;
    }

    public void setHasInsurance(Boolean hasInsurance) {
        this.hasInsurance = hasInsurance;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
