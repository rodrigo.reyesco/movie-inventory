package com.itlg.movieinventory.controller;

import com.itlg.movieinventory.model.Person;
import com.itlg.movieinventory.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/persons")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Person createPerson(@Validated @RequestBody Person person) {
        return personRepository.save(person);
    }

    @GetMapping("/find/{id}")
    public Optional<Person> findPersonById(@PathVariable Long id) {
        return personRepository.findById(id);
    }

    @GetMapping("/find")
    public List<Person> findPersonAll() {
        return personRepository.findAll(Sort.by("lastName"));
    }

    @PostMapping("/find")
    public List<Person> findPersonByName(@RequestBody Person person) {
        return personRepository.findAll(Example.of(person), Sort.by("lastName"));
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public void findPersonAll(@Validated @RequestBody Person person) {
        personRepository.deleteById(person.getId());
    }


}
