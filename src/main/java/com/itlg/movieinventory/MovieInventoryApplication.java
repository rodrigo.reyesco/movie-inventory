package com.itlg.movieinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.itlg.movieinventory.*")
public class MovieInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieInventoryApplication.class, args);
	}

}
